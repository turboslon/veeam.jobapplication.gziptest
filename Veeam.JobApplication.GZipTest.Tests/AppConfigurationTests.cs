﻿using System;
using System.IO;
using NUnit.Framework;

namespace Veeam.JobApplication.GZipTest.Tests
{
    [TestFixture]
    public class AppConfigurationTests
    {
        [Test]
        public void ParsesCompressArgs()
        {
            var filename = Environment.ExpandEnvironmentVariables("%TEMP%\\gziptest.testfile.tmp");
            var filenameOut = filename + ".out";
            File.WriteAllText(filename, "test content");
            var args = new[]
            {
                "compress",
                filename,
                filenameOut
            };

            try
            {
                var config = new AppConfiguration(args);
                Assert.AreEqual(AppConfiguration.OperationType.Compress, config.Operation);
                Assert.AreEqual(filename, config.InputFile);
                Assert.AreEqual(filenameOut, config.OutputFile);
            }
            finally
            {
                File.Delete(filename);
            }
        }
        
        [Test]
        public void GathersComputerInfo()
        {
            var filename = Environment.ExpandEnvironmentVariables("%TEMP%\\gziptest.testfile.tmp");
            var filenameOut = filename + ".out";
            File.WriteAllText(filename, "test content");
            var args = new[]
            {
                "compress",
                filename,
                filenameOut
            };

            try
            {
                var config = new AppConfiguration(args);
                Assert.AreEqual(Environment.ProcessorCount, config.Threads);
            }
            finally
            {
                File.Delete(filename);
            }
        }
        
        [Test]
        public void FailsOnWrongMode()
        {
            var filename = Environment.ExpandEnvironmentVariables("%TEMP%\\gziptest.testfile.tmp");
            var filenameOut = filename + ".out";
            File.WriteAllText(filename, "test content");
            var args = new[]
            {
                "wrong_mode",
                filename,
                filenameOut
            };

            try
            {
                Assert.Throws<ArgumentException>(() => new AppConfiguration(args));
            }
            finally
            {
                File.Delete(filename);
            }
        }
        
        [Test]
        public void BuildsCorrectFileMaps()
        {
            var filename = Environment.ExpandEnvironmentVariables("%TEMP%\\gziptest.testfile.tmp");
            var filenameOut = filename + ".out";
            File.WriteAllText(filename, "test content");
            var args = new[]
            {
                "compress",
                filename,
                filenameOut
            };

            try
            {
                var config = new AppConfiguration(args);
                Assert.IsNotNull(config.InputFileMap);
                Assert.IsNotNull(config.OutputFileMap);
                Assert.AreEqual(1, config.InputFileMap.Blocks);
                Assert.AreEqual(1, config.OutputFileMap.Blocks);
                Assert.IsFalse(config.InputFileMap.Compressed);
                Assert.IsTrue(config.OutputFileMap.Compressed);
            }
            finally
            {
                File.Delete(filename);
            }
        }
        [Test]
        public void FailsOnNullArgs()
        {
            Assert.Throws<ArgumentNullException>(() => new AppConfiguration(null));
        }

        [Test]
        public void FailsOnInsufficientArgs()
        {
            Assert.Throws<ArgumentException>(() => new AppConfiguration(new string[] { }));
            Assert.Throws<ArgumentException>(() => new AppConfiguration(new string[]
            {
            }));
            Assert.Throws<ArgumentException>(() => new AppConfiguration(new string[]
            {
                "compress"
            }));
            Assert.Throws<ArgumentException>(() => new AppConfiguration(new string[]
            {
                "compress",
                "inputfile"
            }));
        }

        [Test]
        public void FailsOnInputFileNotExist()
        {
            var filename = Environment.ExpandEnvironmentVariables("%TEMP%\\gziptest.testfile.tmp");
            var filenameOut = filename + ".out";
            
            if (File.Exists(filename))
                File.Delete(filename);
            
            var args = new[]
            {
                "compress",
                filename,
                filenameOut
            };
            Assert.Throws<FileNotFoundException>(() => new AppConfiguration(args));
        }
    }
}