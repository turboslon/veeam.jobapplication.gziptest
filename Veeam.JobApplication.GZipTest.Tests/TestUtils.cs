﻿using System.IO;

namespace Veeam.JobApplication.GZipTest.Tests
{
    public static class TestUtils
    {
        internal static AppConfiguration BuildAppConfiguration(string filename,
            string filenameOut = null, string mode = "compress")
        {
            if (string.IsNullOrEmpty(filenameOut))
                filenameOut = filename + ".out";
            
            if (mode == "compress")
                File.WriteAllText(filename, "test content");
            
            var args = new[]
            {
                mode,
                filename,
                filenameOut
            };
            var config = new AppConfiguration(args);
            return config;
        }
    }
}