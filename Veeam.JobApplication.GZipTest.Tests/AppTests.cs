﻿using System;
using System.IO;
using NUnit.Framework;

namespace Veeam.JobApplication.GZipTest.Tests
{
    [TestFixture]
    public class AppTests
    {
        [Test]
        public void CreatesWithCorrectConfig()
        {
            var filename = Environment.ExpandEnvironmentVariables("%TEMP%\\gziptest.testfile.tmp");
            var config = TestUtils.BuildAppConfiguration(filename);

            try
            {
                new App(config);
            }
            finally
            {
                File.Delete(filename);
            }
        }

        [Test]
        public void FailsWithNullConfig()
        {
            Assert.Throws<ArgumentNullException>(() => new App(null));
        }

        [Test]
        public void Compresses()
        {
            var filename = Environment.ExpandEnvironmentVariables("%TEMP%\\gziptest.testfile.tmp");
            var config = TestUtils.BuildAppConfiguration(filename);

            try
            {
                var app = new App(config);
                app.Start();
                app.Wait();
            }
            finally
            {
                File.Delete(filename);
            }
        }

        [Test]
        public void Decompresses()
        {
            var filename = Environment.ExpandEnvironmentVariables("%TEMP%\\gziptest.testfile.tmp");
            var config1 = TestUtils.BuildAppConfiguration(filename);

            try
            {
                using (var app1 = new App(config1))
                {
                    app1.Start();
                    app1.Wait();
                }
                
                var config2 = TestUtils.BuildAppConfiguration(filename + ".out", mode: "decompress");
                using (var app2 = new App(config2))
                {
                    app2.Start();
                    app2.Wait();
                }
            }
            finally
            {
                File.Delete(filename);
                File.Delete(filename + ".out");
            }
        }
    }
}