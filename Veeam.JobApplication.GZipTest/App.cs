﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Veeam.JobApplication.GZipTest
{
    internal class App : IDisposable
    {
        private readonly AppConfiguration _config;
        
        private readonly List<IAgent> _agents = new List<IAgent>();
        private bool _isStarted;
        private IInformerAgent Informer { get; set; }

        public App(AppConfiguration config)
        {
            _config = config ?? throw new ArgumentNullException("config");
        }

        public void Start()
        {
            if (_isStarted) throw new InvalidOperationException("Start should be called once for App instance");
            _isStarted = true;
            
            var factory = new AgentFactory(_config);
            Informer = factory.BuildInformerAgent();
            Informer.FatalException += FatalExceptionHandler;
            try
            {
                _agents.Add(factory.BuildReaderAgent());
                _agents.Add(factory.BuildWriterAgent());
                _agents.AddRange(factory.BuildWorkerAgents());
                foreach (var agent in _agents)
                {
                    agent.Start();
                }
            }
            catch (IOException e)
            {
                FatalExceptionHandler(this, new EventArgs<Exception>(e));
            }
        }

        private void FatalExceptionHandler(object sender, EventArgs<Exception> e)
        {        
            Informer.Status("Error occured");
            Informer.Message(e.Arg.Message);
            
            foreach (var agent in _agents)
            {
                agent.Cancel();
                agent.Dispose();
            }
        }

        public void Wait()
        {
            foreach (var agent in _agents)
            {
                agent.Wait();
                agent.Dispose();
            }
        }

        public void Dispose()
        {
            foreach (var agent in _agents)
            {
                agent.Cancel();
                agent.Dispose();
            }
        }
    }
}