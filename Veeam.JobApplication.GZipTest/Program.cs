﻿using System;

namespace Veeam.JobApplication.GZipTest
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var config = new AppConfiguration(args);
                var app = new App(config);
                app.Start();
                app.Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}