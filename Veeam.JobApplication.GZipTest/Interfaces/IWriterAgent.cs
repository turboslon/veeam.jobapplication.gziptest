﻿namespace Veeam.JobApplication.GZipTest
{
    public interface IWriterAgent : IAgent
    {
        void EnqueueBlock(int blockId, byte[] data);
    }
}