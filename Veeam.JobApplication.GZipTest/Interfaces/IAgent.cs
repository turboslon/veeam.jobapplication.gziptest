﻿using System;

namespace Veeam.JobApplication.GZipTest
{
    public interface IAgent : IDisposable
    {
        void Start();
        void Cancel();
        void Wait();
    }
}