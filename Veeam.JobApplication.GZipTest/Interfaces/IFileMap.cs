﻿using System.Collections.Generic;

namespace Veeam.JobApplication.GZipTest
{
    public interface IFileMap
    {
        string Filename { get; }
        long Blocks { get; }
        bool Compressed { get; }
        List<int> Sizes { get; }
        List<long> Offsets { get; }
    }
}