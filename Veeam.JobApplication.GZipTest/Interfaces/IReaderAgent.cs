﻿namespace Veeam.JobApplication.GZipTest
{
    public interface IReaderAgent : IAgent
    {
        Buffer GetNextPage();
    }
}