﻿using System;

namespace Veeam.JobApplication.GZipTest
{
    public interface IInformerAgent
    {
        void Status(string text);
        void Throw(Exception exception);
        void Message(string text);
        event EventHandler<EventArgs<Exception>> FatalException;
    }
}