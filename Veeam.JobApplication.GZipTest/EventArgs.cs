﻿using System;

namespace Veeam.JobApplication.GZipTest
{
    public class EventArgs<T> : EventArgs
    {
        public T Arg { get; }

        public EventArgs(T arg)
        {
            Arg = arg;
        }
    }
}