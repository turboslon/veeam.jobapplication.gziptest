﻿using System;
using System.Threading;

namespace Veeam.JobApplication.GZipTest
{
    internal abstract class ThreadedAgentBase : IAgent
    {
        protected Thread Worker;
        protected volatile bool IsStopped;
        protected readonly AppConfiguration Config;
        protected readonly object SyncRoot = new object();
        protected IInformerAgent Informer { get; }


        protected ThreadedAgentBase(AppConfiguration config, IInformerAgent informer)
        {
            Config = config ?? throw new ArgumentNullException("config");
            Informer = informer;
        }

        public virtual void Start()
        {
            throw new NotImplementedException();
        }

        public virtual void Cancel()
        {
            IsStopped = true;
            Wait();
        }

        public virtual void Wait()
        {
            Worker?.Join();
        }

        public abstract void Dispose();
    }
}