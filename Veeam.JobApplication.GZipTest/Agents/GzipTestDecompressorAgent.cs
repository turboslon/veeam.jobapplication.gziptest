﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace Veeam.JobApplication.GZipTest
{
    internal class GzipTestDecompressorAgent : ThreadedAgentBase
    {
        private readonly IReaderAgent _reader;
        private readonly IWriterAgent _writer;

        public GzipTestDecompressorAgent(AppConfiguration config, 
            IReaderAgent reader, IWriterAgent writer, IInformerAgent informer) : base(config, informer)
        {
            _reader = reader ?? throw new ArgumentNullException("reader");
            _writer = writer ?? throw new ArgumentNullException("writer");
        }


        public override void Start()
        {
            Worker = new Thread(DoWork);
            Worker.Start();
        }

        private void DoWork()
        {
            while (!IsStopped)
            {
                var page = _reader.GetNextPage();
                if (page == null) IsStopped = true;
                else
                {
                    using (var srcMemStream = new MemoryStream(page.Data))
                    using (var gzip = new GZipStream(srcMemStream, CompressionMode.Decompress))
                    using (var tgMemStream = new MemoryStream())
                    {
                        var buffer = new byte[Constants.PageSize];
                        var length = gzip.Read(buffer, 0, buffer.Length);
                        tgMemStream.Write(buffer, 0, length);
                        _writer.EnqueueBlock(page.BlockId, tgMemStream.ToArray());
                    }
                }
            }
        }

        public override void Dispose()
        {
            // Nothing to do: there's no Disposable resources
        }
    }
}