﻿using System;

namespace Veeam.JobApplication.GZipTest
{
    public class ConsoleInformer : IInformerAgent
    {
        public void Status(string text)
        {
            try
            {
                Console.Title = text;
            }
            catch(Exception)
            {
                // swallow all errors
            }
        }

        public void Throw(Exception exception)
        {
            FatalException?.Invoke(this, new EventArgs<Exception>(exception));
        }

        public void Message(string text)
        {
            Console.WriteLine(text);
        }

        public event EventHandler<EventArgs<Exception>> FatalException;
        
    }
}