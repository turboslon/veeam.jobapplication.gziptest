﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace Veeam.JobApplication.GZipTest
{
    internal class GzipTestCompressorAgent : ThreadedAgentBase
    {
        private readonly IReaderAgent _reader;
        private readonly IWriterAgent _writer;

        public GzipTestCompressorAgent(AppConfiguration config,
            IReaderAgent reader, IWriterAgent writer, IInformerAgent informer) : base(config, informer)
        {
            _reader = reader ?? throw new ArgumentNullException("reader");
            _writer = writer ?? throw new ArgumentNullException("writer");
        }


        public override void Start()
        {
            Worker = new Thread(DoWork);
            Worker.Start();
        }

        private void DoWork()
        {
            while (!IsStopped)
            {
                var page = _reader.GetNextPage();
                if (page == null) IsStopped = true;
                else
                {
                    using (var memStream = new MemoryStream())
                    {
                        using (var gzip = new GZipStream(memStream, CompressionMode.Compress))
                        {
                            gzip.Write(page.Data, 0, page.Length);
                        }
                        _writer.EnqueueBlock(page.BlockId, memStream.ToArray());
                    }
                }
            }
        }

        public override void Dispose()
        {
            // Nothing to do: there's no Disposable resources
        }
    }
}