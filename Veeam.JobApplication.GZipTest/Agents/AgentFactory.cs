﻿using System;

namespace Veeam.JobApplication.GZipTest
{
    internal class AgentFactory
    {
        private readonly AppConfiguration _config;
        private readonly object _syncRoot = new object();
        private IInformerAgent _informer;
        private IReaderAgent _reader;
        private IWriterAgent _writer;

        public AgentFactory(AppConfiguration config)
        {
            _config = config ?? throw new ArgumentNullException("config");
        }

        public IInformerAgent BuildInformerAgent()
        {
            if (_informer == null)
                lock (_syncRoot)
                {
                    if (_informer == null) _informer = new ConsoleInformer();
                }

            return _informer;
        }
        
        public IReaderAgent BuildReaderAgent()
        {
            if (_reader == null)
                lock (_syncRoot)
                {
                    if (_reader == null) _reader = new ReaderAgent(_config, _informer);
                }

            return _reader;
        }

        public IWriterAgent BuildWriterAgent()
        {
            BuildInformerAgent();
            if (_writer == null)
                lock (_syncRoot)
                {
                    if (_writer == null) _writer = new WriterAgent(_config, _informer);
                }

            return _writer;
        }

        public IAgent[] BuildWorkerAgents()
        {
            BuildReaderAgent();
            BuildWriterAgent();
            var result = new IAgent[_config.Threads];
            for (int i = 0; i < _config.Threads; i++)
            {
                result[i] = BuildWorkerAgentInternal();
            }

            return result;
        }

        private IAgent BuildWorkerAgentInternal()
        {
            if (_config.Operation == AppConfiguration.OperationType.Compress)
            {
                return new GzipTestCompressorAgent(_config, _reader, _writer, _informer);
            }
            else
            {
                return new GzipTestDecompressorAgent(_config, _reader, _writer, _informer);
            }
        }
    }
}