﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Veeam.JobApplication.GZipTest
{
    internal class WriterAgent : ThreadedAgentBase, IWriterAgent
    {
        private readonly Dictionary<long, byte[]> _toWrite = new Dictionary<long, byte[]>();
        private readonly ReaderWriterLock _lock = new ReaderWriterLock();
        private long _currentBlock;
        private readonly BlockWriter _writer;

        public WriterAgent(AppConfiguration config, IInformerAgent informer) : base(config, informer)
        {
            var writeHeader = Config.Operation == AppConfiguration.OperationType.Compress;
            _writer = new BlockWriter(writeHeader, config.OutputFileMap);
        }

        public override void Start()
        {
            Worker = new Thread(DoWork);
            Worker.Start();
        }

        private void DoWork()
        {
            while (!IsStopped && _currentBlock < Config.OutputFileMap.Blocks)
            {
                _lock.AcquireReaderLock(TimeSpan.FromMinutes(1));
                try
                {
                    if (_toWrite.ContainsKey(_currentBlock))
                    {
                        var block = RemoveBlockByKey();
                        _writer.WriteNextBlock(block, block.Length);
                        _currentBlock += 1;
                        Informer?.Status(string.Format(
                            "Progress: {0:0}%", 100.0 * _currentBlock / Config.OutputFileMap.Blocks));
                    }
                    else
                    {
                        Thread.Sleep(1);
                    }
                }
                finally
                {
                    _lock.ReleaseReaderLock();
                }
            }

            _writer.WriteHeader();
        }

        private byte[] RemoveBlockByKey()
        {
            var cookie = _lock.UpgradeToWriterLock(TimeSpan.FromMinutes(1));
            try
            {
                var block = _toWrite[_currentBlock];
                _toWrite.Remove(_currentBlock);
                return block;
            }
            finally
            {
                _lock.DowngradeFromWriterLock(ref cookie);
            }
        }

        public override void Dispose()
        {
            _writer?.Dispose();
        }

        public void EnqueueBlock(int blockId, byte[] block)
        {
            _lock.AcquireWriterLock(TimeSpan.FromMinutes(1));
            try
            {
                if (_toWrite.ContainsKey(blockId))
                    throw new ArgumentException(string.Format(
                        "blockId {0} already added", blockId));
                _toWrite[blockId] = block;
            }
            finally
            {
                _lock.ReleaseWriterLock();
            }
        }
    }
}