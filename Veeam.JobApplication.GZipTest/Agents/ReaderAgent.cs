﻿using System.Collections.Generic;
using System.Threading;

namespace Veeam.JobApplication.GZipTest
{
    internal class ReaderAgent : ThreadedAgentBase, IReaderAgent
    {
        private readonly BlockReader _reader;
        private readonly Queue<Buffer> _queue = new Queue<Buffer>();
        private int _threshold;
        private long _readyPages;

        public ReaderAgent(AppConfiguration config, IInformerAgent informer) : base(config, informer)
        {
            var fileMap = Config.InputFileMap;
            _threshold = Constants.PageReserveMultiplier * Config.Threads;            
            _reader = new BlockReader(fileMap);
        }

        public override void Start()
        {
            Worker = new Thread(DoWork);
            Worker.Start();
        }

        private void DoWork()
        {
            while (!IsStopped && _reader.CanRead)
            {
                if (Interlocked.Read(ref _readyPages) < _threshold)
                {
                    var buf = _reader.ReadNextBlock();
                    lock (SyncRoot)
                    {
                        _queue.Enqueue(buf);                        
                    }
                    Interlocked.Increment(ref _readyPages);
                }
                else
                {
                    Thread.Sleep(1);
                }
            }

            IsStopped = true;
        }

        public Buffer GetNextPage()
        {
            while (!IsStopped || Interlocked.Read(ref _readyPages) > 0)
            {
                if (Interlocked.Read(ref _readyPages) > 0)
                {
                    lock (SyncRoot)
                    {
                        if (_readyPages > 0)
                        {
                            Interlocked.Decrement(ref _readyPages);                            
                            return _queue.Dequeue();
                        }
                    }

                }
                else
                {
                    Thread.Sleep(1);
                }
            }

            return null;
        }

        public override void Dispose()
        {
            _reader?.Dispose();
        }
    }
}