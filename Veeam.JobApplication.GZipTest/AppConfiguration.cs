﻿using System;
using System.Collections.ObjectModel;
using System.IO;

namespace Veeam.JobApplication.GZipTest
{
    internal class AppConfiguration
    {
        public const int MaxThreads = 100;
        public enum OperationType
        {
            Compress,
            Decompress
        }

        private static readonly Collection<string> OperationTypeStrings = new Collection<string>
        {
            "compress",
            "decompress"
        };

        private const string
            ParametersInfoString = "\nGZip Test for Veeam Job Application\n" +
                                   "Author: Ilya Chidyakin. 2018-07-17\n" +
                                   "Usage:\n" +
                                   "gziptest.exe MODE input_file output_file\n" +
                                   "Where:\n" +
                                   "  MODE: one of the following: compress / decompress\n" +
                                   "  input_file: filename of existing file to compress / decompress\n" +
                                   "  output_file: filename to write the result to";


        public AppConfiguration(string[] args)
        {
            ValidateArguments(args);
            ParseArguments(args);
            BuildFileMaps();
            GatherComputerInfo();
        }

        private void BuildFileMaps()
        {
            var builder = new FileMapBuilder();
            if (Operation == OperationType.Compress)
            {
                InputFileMap = builder.BuildPlainFileMap(InputFile);
                OutputFileMap = builder.PrepareGzipTestFileMapFromPlain(InputFileMap, OutputFile);
            }
            else
            {
                InputFileMap = builder.BuildGzipTestFileMap(InputFile);
                OutputFileMap = builder.PreparePlainFileMapFromGzipTest(InputFileMap, OutputFile);
            }
        }

        private int _threads;

        public OperationType Operation { get; private set; }
        public string InputFile { get; private set; }
        public string OutputFile { get; private set; }

        public int Threads
        {
            get { return _threads; }
            set
            {
                if (value > 0 && value <= MaxThreads)
                    _threads = value;
                else
                {
                    throw new ArgumentException(string.Format(
                        "Unable to set Threads to {0}. Expected between {1} and {2}",
                        value, 1, MaxThreads));
                }
            }
        }

        public IFileMap InputFileMap { get; set; }
        public IFileMap OutputFileMap { get; set; }

        private void GatherComputerInfo()
        {
            Threads = Environment.ProcessorCount;
        }

        private void ParseArguments(string[] args)
        {
            Operation = (OperationType) Enum.Parse(typeof(OperationType), args[0], true);
            InputFile = args[1];
            OutputFile = args[2];
        }

        private void ValidateArguments(string[] args)
        {
            if (args == null) throw new ArgumentNullException("args");
            if (args.Length != 3) throw new ArgumentException(ParametersInfoString);
            if (!OperationTypeStrings.Contains(args[0].ToLowerInvariant()))
                throw new ArgumentException(ParametersInfoString);
            if (!File.Exists(args[1])) throw new FileNotFoundException(ParametersInfoString);
        }
    }
}