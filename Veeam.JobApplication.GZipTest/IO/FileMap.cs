﻿using System;
using System.Collections.Generic;

namespace Veeam.JobApplication.GZipTest
{        
    public class FileMap : IFileMap
    {
        public string Filename { get; }
        public long Blocks { get; }
        public bool Compressed { get; }
        public List<int> Sizes { get; }
        public List<long> Offsets { get; }

        internal FileMap(string filename, bool compressed, BlockInfoStruct[] blocks)
        {
            Filename = filename ?? throw new ArgumentNullException("filename");
            Compressed = compressed;
            if (blocks == null) throw new ArgumentNullException("blocks");
            if (blocks.Length == 0) throw new ArgumentException("blocks should contain at least one element");
            Sizes = new List<int>(blocks.Length);
            Offsets = new List<long>(blocks.Length);
            for (int i = 0; i < blocks.Length; i++)
            {
                var blockInfo = blocks[i];
                Sizes.Add(Convert.ToInt32(blockInfo.Size));
                Offsets.Add(blockInfo.Offset);
            }

            Blocks = blocks.Length;
        }
    }

    internal struct BlockInfoStruct
    {
        public long Id;
        public long Offset;
        public long Size;
    }
}