﻿using System;

namespace Veeam.JobApplication.GZipTest
{
    public interface IBlockReader : IDisposable
    {
        long TotalBlocks { get; }
        Buffer ReadNextBlock();
    }
}