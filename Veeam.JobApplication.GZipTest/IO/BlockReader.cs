﻿using System;
using System.IO;

namespace Veeam.JobApplication.GZipTest
{
    public class BlockReader : IBlockReader
    {
        private int _currentBlock;
        private IFileMap FileMap { get; set; }
        private FileStream SourceStream { get; set; }


        public long TotalBlocks
        {
            get { return FileMap.Blocks; }
        }

        public bool CanRead
        {
            get { return SourceStream.Position < SourceStream.Length; }
        }

        public BlockReader(IFileMap fileMap)
        {
            FileMap = fileMap ?? throw new ArgumentNullException("fileMap");
            SourceStream = new FileStream(FileMap.Filename, FileMode.Open, FileAccess.Read, FileShare.Read,
                Constants.ReadBufferSize);

            _currentBlock = 0;
            SourceStream.Seek(FileMap.Offsets[_currentBlock], SeekOrigin.Begin);
        }

        public Buffer ReadNextBlock()
        {
            if (_currentBlock < TotalBlocks)
            {
                var blockSize = FileMap.Sizes[_currentBlock];
                var buf = new byte[blockSize];
                var buffer = new Buffer(_currentBlock, blockSize, buf);
                var read = SourceStream.Read(buf, 0, blockSize);
                if (read != blockSize)
                    throw new InvalidOperationException(string.Format(
                        "Expected to read {0} bytes but read {1}", blockSize, read));
                _currentBlock++;
                return buffer;
            }

            throw new InvalidOperationException(string.Format("Already read all {0} blocks", TotalBlocks));
        }

        public void Dispose()
        {
            SourceStream?.Dispose();
            SourceStream = null;
            GC.SuppressFinalize(this);
        }
    }
}