﻿using System;
using System.IO;

namespace Veeam.JobApplication.GZipTest
{
    public class BlockWriter
    {
        private readonly bool _writeHeader;
        private volatile int _currentBlock;

        private IFileMap FileMap { get; set; }
        private FileStream TargetStream { get; set; }


        public long TotalBlocks
        {
            get { return FileMap.Blocks; }
        }

        public long WrittenBlocks
        {
            get { return _currentBlock; }
        }

        public BlockWriter(bool writeHeader, IFileMap fileMap)
        {
            _writeHeader = writeHeader;
            FileMap = fileMap ?? throw new ArgumentNullException("fileMap");
            TargetStream = new FileStream(FileMap.Filename, FileMode.Create, FileAccess.Write, FileShare.None,
                Constants.ReadBufferSize);

            _currentBlock = 0;
            WriteHeader();
        }

        public void WriteHeader()
        {
            if (!_writeHeader) return;
            
            using (var ms = new MemoryStream())
            {
                ms.Write(BitConverter.GetBytes(Constants.GZippedFileMagicNumber), 0, sizeof(long));
                ms.Write(BitConverter.GetBytes(FileMap.Blocks), 0, sizeof(long));
                for (int i = 0; i < FileMap.Blocks; i++)
                {
                    ms.Write(BitConverter.GetBytes(FileMap.Offsets[i]), 0, sizeof(long));
                    ms.Write(BitConverter.GetBytes((long) FileMap.Sizes[i]), 0, sizeof(long));
                }

                var data = ms.ToArray();
                TargetStream.Seek(0, SeekOrigin.Begin);
                TargetStream.Write(data, 0, data.Length);
            }
        }

        public void WriteNextBlock(byte[] data, int length)
        {
            FileMap.Offsets[_currentBlock] = TargetStream.Position;
            FileMap.Sizes[_currentBlock] = length;
            TargetStream.Write(data, 0, length);
            _currentBlock++;
        }

        public void Dispose()
        {
            TargetStream?.Dispose();
            TargetStream = null;
            GC.SuppressFinalize(this);
        }
    }
}