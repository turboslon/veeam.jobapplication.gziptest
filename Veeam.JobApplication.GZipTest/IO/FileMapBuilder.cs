﻿using System;
using System.IO;

namespace Veeam.JobApplication.GZipTest
{
    internal class FileMapBuilder
    {
        const int SizeofLong = sizeof(long);

        public IFileMap BuildPlainFileMap(string filename)
        {
            var size = new FileInfo(filename).Length;
            var blocksCount = Convert.ToInt64(Math.Ceiling((double) size / Constants.PageSize));
            var blocks = new BlockInfoStruct[blocksCount];
            for (int i = 0; i < blocksCount; i++)
            {
                blocks[i].Id = i;
                blocks[i].Offset = (long)i * Constants.PageSize;
                blocks[i].Size = Math.Min(size - blocks[i].Offset, Constants.PageSize);
            }

            return new FileMap(filename, false, blocks);
        }

        public IFileMap BuildGzipTestFileMap(string filename)
        {
            using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read, 8096))
            {
                var blocksCount = ResolveGzipTestBlocksCount(filename, stream);
                return ResolveGzipTestBlocksData(filename, blocksCount, stream);
            }
        }

        public IFileMap PrepareGzipTestFileMapFromPlain(IFileMap fileMap, string targetFile)
        {
            var blocks = new BlockInfoStruct[fileMap.Blocks];
            for (int i = 0; i < blocks.Length; i++)
            {
                blocks[i].Id = i;
                blocks[i].Offset = 0;
                blocks[i].Size = 0;
            }

            return new FileMap(targetFile, true, blocks);
        }

        public IFileMap PreparePlainFileMapFromGzipTest(IFileMap fileMap, string targetFile)
        {
            var blocks = new BlockInfoStruct[fileMap.Blocks];
            for (int i = 0; i < blocks.Length; i++)
            {
                blocks[i].Id = i;
                blocks[i].Offset = i * Constants.PageSize;
                blocks[i].Size = Constants.PageSize;
            }

            return new FileMap(targetFile, true, blocks);
        }

        private static long ResolveGzipTestBlocksCount(string filename, FileStream stream)
        {
            var bytes = new byte[2 * SizeofLong];
            // TODO: Implement read checks
            stream.Read(bytes, 0, bytes.Length);
            var magicNumber = BitConverter.ToInt64(bytes, 0);
            if (magicNumber != Constants.GZippedFileMagicNumber)
                throw new InvalidOperationException(
                    string.Format("File {0} has wrong GZipTest magic number. Probably it isn't compressed by GZipTest", filename));

            var blocksCount = BitConverter.ToInt64(bytes, SizeofLong);
            return blocksCount;
        }

        private static IFileMap ResolveGzipTestBlocksData(string filename, long blocksCount, FileStream stream)
        {
            var data = new byte[blocksCount * 2 * SizeofLong];
            var blocks = new BlockInfoStruct[blocksCount];
            // TODO: Implement read checks
            stream.Read(data, 0, data.Length);

            for (int i = 0; i < blocksCount; i++)
            {
                blocks[i].Id = i;
                blocks[i].Offset = BitConverter.ToInt64(data, 2 * i * SizeofLong);
                blocks[i].Size = BitConverter.ToInt64(data, (2 * i + 1) * SizeofLong);
            }

            return new FileMap(filename, true, blocks);
        }
    }
}