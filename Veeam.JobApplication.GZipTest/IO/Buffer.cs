﻿namespace Veeam.JobApplication.GZipTest
{
    public class Buffer
    {
        public int Length { get;  }
        public byte[] Data { get; }
        public int BlockId { get; }

        public Buffer(int blockId, int length, byte[] data)
        {
            Length = length;
            BlockId = blockId;
            Data = data;
        }
    }
}