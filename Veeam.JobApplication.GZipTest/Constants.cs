﻿namespace Veeam.JobApplication.GZipTest
{
    public class Constants
    {
        public const int PageReserveMultiplier = 4;
        public const int PageSize = 8 * 1024 * 1024; // 8 MB
        public const int ReadBufferSize = 4 * PageSize; // 32 MB
        public const long GZippedFileMagicNumber = 16384;

    }
}